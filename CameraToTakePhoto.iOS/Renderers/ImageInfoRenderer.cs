﻿using System;
using System.ComponentModel;
using CameraToTakePhoto.iOS.Renderers;
using CameraToTakePhoto.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Photos;
using CoreFoundation;
using ImageIO;
using System.Diagnostics;

[assembly: ExportRenderer(typeof(ImageInfo), typeof(ImageInfoRenderer))]
namespace CameraToTakePhoto.iOS.Renderers
{
    public class ImageInfoRenderer : ImageRenderer
    {
        public ImageInfoRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if(e.PropertyName == Image.SourceProperty.PropertyName)
            {
                GetInfoImage();
            }
        }

        private void GetInfoImage()
        {
            //var a = Element.Source;

            //if (Control == null || Control.Image == null) return;

            //var data = Control.Image.AsJPEG(1f);
            //if (data == null) return;
            //var source = CGImageSource.FromData(data, null);
            //if (source == null) return;

            //// Get type
            //var type = source.TypeIdentifier;
            //Debug.WriteLine($"type======={type}======");

            //// Get properties
            //var op = new CGImageOptions()
            //{
            //    ShouldAllowFloat = true,
            //    ShouldCache = true,
            //    ShouldCacheImmediately = true,
            //};
            //var properties = source.CopyProperties(op);
            //Debug.WriteLine($"properties======={properties}======");

            //var ciimage = Control.Image.CIImage;
            //var prop = ciimage.Properties;
            //var aaaa = prop.Gps;

            //Debug.WriteLine($"properties======={aaaa.Latitude}, {aaaa.Longitude}======");
            
        }
    }
}
