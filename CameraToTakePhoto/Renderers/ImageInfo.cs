﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace CameraToTakePhoto.Renderers
{
    public class ImageInfo : Image
    {
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string info { get; internal set; }

        public void setInfo( string info)
        {
            this.info = info;
        }
    }
}
