﻿using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Plugin.Media
{
    /// <summary>
    /// Cross platform Media implemenations
    /// </summary>
    public class CrossMedia
    {

        static Lazy<CrossMedia> crossMediaLazy = new Lazy<CrossMedia>(() => new CrossMedia(), System.Threading.LazyThreadSafetyMode.PublicationOnly);
        public static CrossMedia Instance => crossMediaLazy.Value;

        [Obsolete]
        private async Task<bool> CheckCameraPermission()
        {
            var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
            cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            if (cameraStatus == PermissionStatus.Granted)
            {
                return true;
            }
            return false;
        }

        [Obsolete]
        private async Task<bool> CheckStoragePermission()
        {
            var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
            cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            if (cameraStatus == PermissionStatus.Granted)
            {
                return true;
            }
            return false;
        }

        public bool IsCameraAvailable()
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.IsCameraAvailable;            
        }

        public bool IsTakePhotoSupported()
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.IsTakePhotoSupported;
        }

        public bool IsPickPhotoSupported()
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.IsPickPhotoSupported;
        }

        public bool IsTakeVideoSupported()
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.IsTakeVideoSupported;
        }

        public bool IsPickVideoSupported()
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.IsPickVideoSupported;
        }
        

        public Task<MediaFile> TakePhotoAsync(StoreCameraMediaOptions options, CancellationToken token = default(CancellationToken))
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.TakePhotoAsync(options, token);
        }

        public Task<MediaFile> PickPhotoAsync(PickMediaOptions options = null, CancellationToken token = default(CancellationToken))
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.PickPhotoAsync(options, token);
        }


        public Task<MediaFile> PickVideoAsync(CancellationToken token = default(CancellationToken))
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.PickVideoAsync(token);
        }

        public Task<MediaFile> TakeVideoAsync(StoreVideoOptions options, CancellationToken token = default(CancellationToken))
        {
            IMedia service = DependencyService.Get<IMedia>();
            return service.TakeVideoAsync(options, token);
        }


        //        static Lazy<IMedia> implementation = new Lazy<IMedia>(() => CreateMedia(), System.Threading.LazyThreadSafetyMode.PublicationOnly);

        //        /// <summary>
        //		/// Gets if the plugin is supported on the current platform.
        //		/// </summary>
        //		public static bool IsSupported => implementation.Value == null ? false : true;

        //        /// <summary>
        //        /// Current plugin implementation to use
        //        /// </summary>
        //        public static IMedia Current
        //        {
        //            get
        //            {
        //                var ret = implementation.Value;
        //                if (ret == null)
        //                {
        //                    throw NotImplementedInReferenceAssembly();
        //                }
        //                return ret;
        //            }
        //        }

        //        static IMedia CreateMedia()
        //        {
        //#if NETSTANDARD1_0 || NETSTANDARD2_0
        //            return null;
        //#else
        //            return new MediaImplementation();
        //#endif
        //        }

        //        internal static Exception NotImplementedInReferenceAssembly() =>
        //            new NotImplementedException("This functionality is not implemented in the portable version of this assembly.  You should reference the NuGet package from your main application project in order to reference the platform-specific implementation.");

    }
}
