﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using Xamarin.Essentials;
using Media = Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace CameraToTakePhoto
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void TakePhoto_Clicked(object sender, EventArgs e)
        {
            if (!CrossMedia.Instance.IsCameraAvailable() || !CrossMedia.Instance.IsTakePhotoSupported())
            {
                await DisplayAlert("No Camera", ":( No camera available.", "OK");
                return;
            }

            var currentLocation = await Geolocation.GetLastKnownLocationAsync();
           var file = await CrossMedia.Instance.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear,
                Location = new Media.Location() { Latitude = currentLocation != null ? currentLocation.Latitude : 0, Longitude = currentLocation != null ? currentLocation.Longitude : 0 }
            });

            if (file == null)
                return;
            await DisplayAlert("File Location", file.Path, "OK");
            if (file.options != null && file.options.Location != null)
            {
                Debug.WriteLine($"Location: {file.options.Location.Latitude} - {file.options.Location.Longitude}"); 
                lblLat.Text = $"Latitude: {file.options.Location.Latitude.ToString()}";
                lblLong.Text = $"Longitude: {file.options.Location.Longitude.ToString()}";
            }
            else
            {
                Debug.WriteLine($"No location");
                lblLat.Text = $"No location";
            }

            image.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }

        private async void PickPhoto_Clicked(object sender, EventArgs e)
        {
            if (!CrossMedia.Instance.IsPickPhotoSupported())
            {
                await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            var file = await Plugin.Media.CrossMedia.Instance.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,

            });

            if (file == null)
                return;
            if (file.options != null && file.options.Location != null)
            {
                Debug.WriteLine($"Location: {file.options.Location.Latitude} - {file.options.Location.Longitude}");
                lblLat.Text = $"Latitude: {file.options.Location.Latitude.ToString()}";
                lblLong.Text = $"Longitude: {file.options.Location.Longitude.ToString()}";
            }
            else
            {
                Debug.WriteLine($"No location");
                lblLat.Text = $"No location";
                lblLong.Text = "";
            }
            image.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
        }

        private async void TakeVideo_Clicked(object sender, EventArgs e)
        {
            if (!CrossMedia.Instance.IsCameraAvailable() || !CrossMedia.Instance.IsTakeVideoSupported())
            {
                await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }

            var file = await CrossMedia.Instance.TakeVideoAsync(new Plugin.Media.Abstractions.StoreVideoOptions
            {
                Name = "video.mp4",
                Directory = "DefaultVideos",
            });

            if (file == null)
                return;

            await DisplayAlert("Video Recorded", "Location: " + file.Path, "OK");

            file.Dispose();
        }

        private async void PickVideo_Clicked(object sender, EventArgs e)
        {
            if (!CrossMedia.Instance.IsPickVideoSupported())
            {
                await DisplayAlert("Videos Not Supported", ":( Permission not granted to videos.", "OK");
                return;
            }
            var file = await CrossMedia.Instance.PickVideoAsync();

            if (file == null)
                return;

            await DisplayAlert("Video Selected", "Location: " + file.Path, "OK");
            file.Dispose();
        }
    }
}
